#!/usr/bin/env bash

docker-machine create \
--driver=hetzner \
--hetzner-image=centos-7 \
--hetzner-api-token=HETZNER_TOKEN \
--hetzner-server-type=cx21 \
atlas-service

eval "$(docker-machine env atlas-service)"

docker-compose up -d
