// server.js
'use strict'

module.exports = async function (fastify, opts) {
  fastify
  .register(require('fastify-websocket'))
  .after(routes);

  function routes(){
    fastify.register(require('./js/routes/machine-create'));
    fastify.register(require('./js/routes/machine-deploy'));
    return fastify;
  }
}
