const Machine = require('../lib/docker');
const compose = require('../lib/compose');

async function doDeploy(env, domain, email, socket){
  await compose(env, domain, email, {
    send(data){
      socket.send(JSON.stringify(data));
    }
  });
  socket.send(JSON.stringify({ success: true }));
}

async function routes (fastify, options) {
  fastify.get('/ws/machine-deploy', { websocket: true }, (conn, req) => {
    console.debug('Deploy Atlassian containers');
    conn.socket.on('message', async (msg) => {
      try {
        const data = typeof msg === 'string'? JSON.parse(msg): msg;
        const machine = new Machine(data.name);
        const info = await machine.inspect();
        if (info.driver.accessToken !== data.token){
          throw Error('Invalid token');
        }
        const env = await machine.env({ parse: true });
        await doDeploy(env, data.domain, data.email, conn.socket);
      } catch(err) {
        console.error(err);
        conn.socket.send(JSON.stringify({ error: err.message }));
      } finally {
        conn.end();
      }
    });
  });
}

module.exports = routes;
