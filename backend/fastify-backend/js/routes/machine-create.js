const Machine = require('../lib/docker');
const { delay } = require('../lib/util');

async function doCreate(name, token, socket){
  await Machine.create(name, 'hetzner', { 'hetzner-api-token': token, 'hetzner-server-type': 'cx41' }, {
    send(data){
      socket.send(JSON.stringify(data));
    }
  });
  const machine = new Machine(name);
  const ip = await machine.ip();
  socket.send(JSON.stringify({ success: true, name, ip }));
}

async function routes (fastify, options) {
  fastify.get('/ws/machine-create', { websocket: true }, (conn, req) => {
    console.debug('Create docker machine');
    const name = `atlas-${Date.now()}`;

    conn.socket.on('message', async (msg) => {
      try {
        const data = typeof msg === 'string'? JSON.parse(msg): msg;
        await doCreate(name, data.token, conn.socket);
      } catch(err) {
        console.error(err);
        conn.socket.send(JSON.stringify({ error: err.message }));
      } finally {
        conn.end();
      }
    });
  });
}

module.exports = routes;
