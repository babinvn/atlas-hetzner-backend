'use strict'

const { extend } = require('underscore');
const fs = require('fs');
const { join } = require('path');
const { spawn } = require('child_process');

async function compose (env, domain, email, socket) {
  const template = join(__dirname, 'docker-compose.yml.template');
  let yml = fs.readFileSync(template, 'utf-8');
  yml = yml.replace(/\$\{DOMAIN\}/g, domain).replace(/\$\{EMAIL\}/g, email);

  return new Promise((resolve, reject) => {
    const opts = {
      env: extend({}, process.env, env),
      encoding: 'utf8'
    };

    const dockerCompose = spawn('docker-compose', [ '-f', '-', 'up', '-d' ], opts);
    dockerCompose.on('close', (code) => {
      if (code !== 0) {
        reject(`docker-compose exited with error code ${code}`);
      } else {
        resolve();
      }
    });

    if (socket){
      dockerCompose.stdout.setEncoding('utf-8').on('data', (data) => {
        socket.send({ stdout: data });
      });
      dockerCompose.stderr.setEncoding('utf-8').on('data', (data) => {
        socket.send({ stderr: data });
      });
    }
    dockerCompose.stdin.write(yml)
    dockerCompose.stdin.end();
  });
}

module.exports = compose
