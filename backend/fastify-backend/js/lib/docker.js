'use strict'

const env = process.env
const cp = require('child_process')
const camelCase = require('camel-case')
const hasOwnProperty = Object.prototype.hasOwnProperty

const HOST_NON_EXISTENT = /host does not exist/i
const ALREADY_RUNNING = /already running/i
const ALREADY_STOPPED = /already stopped/
const NEWLINE = /\r?\n/
const LIST_COLUMNS_SEP = ','

const LIST_COLUMNS =
  ['Name',
    'Active',
    'ActiveHost',
    'ActiveSwarm',
    'DriverName',
    'State',
    'URL',
    'Swarm',
    'Error',
    'DockerVersion',
    'ResponseTime']

class Machine {
  constructor (opts) {
    opts = Machine.options(opts)
    this.name = opts.name || env.DOCKER_MACHINE_NAME || 'default'
  }

  static options (opts) {
    if (typeof opts === 'string') return { name: opts }
    else return opts || {}
  }

  static command (args, done, socket) {
    const { stdout, stderr } = cp.execFile('docker-machine', [].concat(args), {
      cwd: env.DOCKER_TOOLBOX_INSTALL_PATH || '.',
      encoding: 'utf8'
    }, done);

    if (socket){
      stdout.on('data', (data) => {
        socket.send({ stdout: data });
      });
      stderr.on('data', (data) => {
        socket.send({ stderr: data });
      });
    }
  }

  static async ip (name) {
    return new Promise((resolve, reject) => {
      Machine.command(['ip', name], (err, stdout) => {
        if (err){
          reject(err);
        } else {
          resolve(stdout.trim());
        }
      });
    });
  }
  static async status (name) {
    return new Promise((resolve, reject) => {
      Machine.command(['status', name], (err, stdout) => {
        if (err){
          reject(err);
        } else {
          resolve(stdout.trim().toLowerCase());
        }
      });
    });
  }
  static async create (name, driver, options, socket) {
    if (typeof name !== 'string' || name === '') {
      throw new TypeError('name is required');
    }

    if (typeof driver !== 'string' || driver === '') {
      throw new TypeError('driver is required');
    }

    const args = ['create', '--driver', driver];

    for (const key in options) {
      if (hasOwnProperty.call(options, key)) {
        args.push(`--${key}`, options[key]);
      }
    }

    args.push(name);

    return new Promise((resolve, reject) => {
      Machine.command(args, (err) => {
        if (err){
          reject(err);
        } else {
          resolve();
        }
      }, socket);
    });
  }
  static async env (name, opts) {
    const args = ['env'];

    if (opts.parse)
      args.push('--shell', 'bash');
    else if (opts.shell)
      args.push('--shell', opts.shell);

    args.push(name);

    return new Promise((resolve, reject) => {
      Machine.command(args, function (err, stdout) {
        if (err) {
          reject(err);
        } else {
          if (opts.parse) {
            const res = {};
            stdout.split(/\n+/).forEach(line => {
              const m = /^export (.+)="([^"]+)/i.exec(line);
              if (m) res[m[1]] = m[2];
            });
            resolve(res);
          } else {
            resolve(stdout.trim());
          }
        }
      })
    });
  }
  static async inspect (name) {
    return new Promise((resolve, reject) => {
      Machine.command(['inspect', name], (err, stdout) => {
        if (err) {
          reject(err);
        } else {
          try {
            const data = JSON.parse(stdout.trim());
            resolve(merge({}, data));
          } catch (err) {
            reject(err);
          }
        }
      });
    });
  }
  static async ssh (name, cmd) {
    if (Array.isArray(cmd)) {
      cmd = cmd.join(' ');
    } else if (typeof cmd !== 'string') {
      throw new TypeError('Command must be an array or string');
    }
    cmd = cmd.trim()
    if (!cmd) throw new TypeError('Command may not be empty');

    return new Promise((resolve, reject) => {
      Machine.command(['ssh', name, cmd], (err, stdout) => {
        if (err) {
          reject(err);
        } else {
          resolve(stdout.trim());
        }
      });
    });
  }
  static async scp (name, source, target) {
    return new Promise((resolve, reject) => {
      Machine.command(['scp', source, `${name}:${target}`], (err, stdout) => {
        if (err) {
          reject(err);
        } else {
          resolve(stdout.trim());
        }
      });
    });
  }
}
;[ 'ip', 'status', 'env', 'inspect', 'ssh', 'scp' ].forEach(method => {
  Machine.prototype[method] = async function () {
    const args = Array.from(arguments)
    args.unshift(this.name)
    return Machine[method].apply(null, args);
  }
})

module.exports = Machine

function merge (node, data) {
  for (const key in data) {
    const val = data[key]
    node[camelCase(key)] = isObject(val) ? merge({}, val) : val
  }

  return node
}

function isObject (obj) {
  return typeof obj === 'object' && obj !== null
}
