# atlas-hetzner-backend
Fastify backend for [atlassian-hetzner-wizrd](https://github.com/babinvn/atlassian-hetzner-wizard)

First build base image for backend server. This creates a node-enabled image with docker-machine and docker-compose installed:
```
cd $PROJECT/backend/base-image
docker build -t atlas-hetzner-base .
```

Edit file `$PROJECT/backend/fastify-backend/js/lib/docker-compose.yml.template` and replace `${REGISTRY}` with real public registry where you pushed your custom nginx and postgres images from [atlassian-hetzner](https://github.com/babinvn/atlassian-hetzner). Or use mesilat public repo:
```
services:
  nginx:
    image: registry.mesilat.com/slava/atlas-nginx
    ...
  postgres:
    image: registry.mesilat.com/slava/atlas-postgres:1.1
    ...
```
Other variables (`${DOMAIN}` and `${EMAIL}`) should remain unchaged -- they will be replaced by backend when your Confluence/JIRA are deployed.

Install required node packages and build the server image:
```
cd $PROJECT/backend/fastify-backend
yarn install
cd $PROJECT/backend
docker build -t atlas-backend .
```

Build Nginx image configured for websockets proxy:
```
cd $PROJECT/nginx
docker build -t atlas-service-nginx .
```

In `$PROJECT/docker-compose.yml` replace `${DOMAIN}` and `${EMAIL}` with your backend server domain and email - these will be used to create SSL certificate.

Create a Hetzner server and deploy:
```
cd $PROJECT
docker-machine create \
--driver=hetzner \
--hetzner-image=centos-7 \
--hetzner-api-token=HETZNER_TOKEN \
--hetzner-server-type=cx21 \
atlas-service

eval "$(docker-machine env atlas-service)"

docker-compose up -d
```

Your backend server endpoint will be available at address: `wss://${DOMAIN}/ws`; use it with your instance of [atlassian-hetzner-wizard](https://github.com/babinvn/atlassian-hetzner-wizard)
